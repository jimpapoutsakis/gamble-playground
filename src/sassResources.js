const path = require("path");

const resources = [
  'assets/sass/gamble-playground-module.scss'
];


module.exports = resources.map(file => path.resolve(__dirname, file));