import ButtonStyle from '../Button.style.scss';
import OutlineWrapperStyle from './OutlineWrapper.style.scss';
import classNames from 'classnames';

const OutlineWrapper = () => (
    <div className={classNames([OutlineWrapperStyle.outlineWrapper])} >

        {/* Top Part */}
        <div className={
            classNames([
                OutlineWrapperStyle.outlinePart,
                OutlineWrapperStyle.outlineTopPart
            ])
        } >
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
        </div>

        {/* Bottom Part */}
        <div
            className={
                classNames([
                    OutlineWrapperStyle.outlinePart,
                    OutlineWrapperStyle.outlineBottomPart
                ])
            } >
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
            <div className={classNames([OutlineWrapperStyle.outlineLine, ButtonStyle.outlineLine])} />
        </div>
    </div>
);

export default OutlineWrapper;