import ButtonStyle from '../Button.style.scss';

export default (variant, shape, outline, size) => {
    let stylesArr = [],
     variantClass = '', 
     shapeClass = '', 
     outlineClass = '', 
     sizeClass = '';

    // Variant
    switch (variant) {
        case 'secondary':
            variantClass = ButtonStyle.btnSecondary;
            outlineClass = ButtonStyle.btnSecondaryOutline;
            break;
        case 'success':
            variantClass = ButtonStyle.btnSuccess;
            outlineClass = ButtonStyle.btnSuccessOutline;
            break;
        case 'warning':
            variantClass = ButtonStyle.btnWarning;
            outlineClass = ButtonStyle.btnWarningOutline;
            break;
        case 'danger':
            variantClass = ButtonStyle.btnDanger;
            outlineClass = ButtonStyle.btnDangerOutline;
            break;
        case 'info':
            variantClass = ButtonStyle.btnInfo;
            outlineClass = ButtonStyle.btnInfoOutline;
            break;
        default:
            variantClass = ButtonStyle.btnPrimary;
            outlineClass = ButtonStyle.btnPrimaryOutline;
            break;
    };

    // Outline
    const outlineStyles = [
        ButtonStyle.btnOutline,
        outlineClass
    ];

    // Size
    switch (size) {
        case 'sm':
            sizeClass = ButtonStyle.btnSm;
            break;
        case 'lg':
            sizeClass = ButtonStyle.btnLg;
            break;
    };

    // Shape
    shapeClass = ( shape == 'fab' ? ButtonStyle.fab : ButtonStyle.normal);

    stylesArr = [
        variantClass,
        shapeClass,
        outline && outlineStyles,
        sizeClass
    ];

    return stylesArr;

};