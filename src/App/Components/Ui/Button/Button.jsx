import { TransitionGroup, CSSTransition } from 'react-transition-group';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Ink from 'react-ink';
import { OutlineWrapper } from './OutlineWrapper';
import ButtonStyle from './Button.style.scss';
import { GetButtonStyle } from './Helpers';

class Button extends React.Component {

    static defaultProps = () => ({
        disabled: false,
        outline: false,
        type: 'button',
        size: ''
    })

    static propTypes = {
        disabled: PropTypes.bool,
        outline: PropTypes.bool,
        type: PropTypes.string,
        size: PropTypes.string,
        icon: PropTypes.string
    }

    RenderClickEffect = () => !this.props.disabled && <Ink />

    RenderOutline = () => this.props.outline && <OutlineWrapper />

    RenderDisabledIcon = disabledProp => (
        <CSSTransition
            enter={true}
            appear={false}
            exit={true}
            timeout={250}
            in={disabledProp}
            classNames={{
                enter: ButtonStyle.btnDisabledIconEnter,
                enterActive: ButtonStyle.btnDisabledIconEnteActive,
                enterDone: ButtonStyle.btnDisabledIconEnterDone,
                exit: ButtonStyle.btnDisabledIconExit,
                exitActive: ButtonStyle.btnDisabledIconExit,
                exitDone: ButtonStyle.btnDisabledIconExitDone
            }}
        >
            <i className={
                classNames([
                    ButtonStyle.btnDisabledIcon,
                    'fa fa-lock'
                ])
            }
            />
        </CSSTransition>
    )

    RenderButtonIcon = () => {
        const { icon } = this.props;
        return (
            icon && <i className={classNames([ButtonStyle.btnIcon, this.props.icon])} />
        );
    };

    GetDisabledClass = () => this.props.disabled == null && ButtonStyle.btnEnabled;

    GenerateProps = () => {
        const { type, disabled, className, variant, shape, outline, size, ...rest } = this.props;
        return {
            type: this.props.type,
            disabled: this.props.disabled,
            className: classNames([
                ButtonStyle.btn,
                this.GetDisabledClass(),
                this.props.className,
                GetButtonStyle(variant, shape, outline, size)
            ]),
            ...rest
        };
    }

    render() {
        return (
            <button {...this.GenerateProps()}>

                {this.RenderOutline()}

                <div className={`${ButtonStyle.btnContent}`} >

                    {this.RenderClickEffect()}

                    <span className={`${ButtonStyle.btnText}`}>

                        {this.RenderButtonIcon()}

                        {this.props.children}
                    </span>

                    {this.RenderDisabledIcon(this.props.disabled)}
                </div>
            </button>

        );
    }
}

export default Button;