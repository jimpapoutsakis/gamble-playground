import classNames from 'classnames';
import PropTypes from 'prop-types';
import CircleTagStyle from './CircleTag.style.scss';

const CircleTag = ({
    children,
    className,
    bgColorClass,
    borderColorClass,
    position,
    width,
    height,
    padding,
    borderWidth,
    borderStyle,
    ...rest
}) => (
        <div
            style={{
                width,
                height,
                padding,
                borderStyle,
                borderWidth,
                ...position
            }}
            className={classNames([
                className,
                bgColorClass,
                borderColorClass,
                CircleTagStyle.circleTag,
            ])}
            {...rest}
        >
            {children}
        </div>
    );

CircleTag.propTypes = {
    bgColorClass: PropTypes.string,
    borderColorClass: PropTypes.string,
    position: PropTypes.object,
    width: PropTypes.string,
    height: PropTypes.string,
    padding: PropTypes.string,
    borderWidth: PropTypes.string,
    borderStyle: PropTypes.string
};

CircleTag.defaultProps = {
    bgColorClass: 'bg-primary',
    borderColorClass: 'border-secondary',
    position: {},
    width: "55px",
    height: "55px",
    padding: "10px",
    borderWidth: '2px',
    borderStyle: 'dotted'
};

export default CircleTag;