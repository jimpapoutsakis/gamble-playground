import classNames from 'classnames';
import PropTypes from 'prop-types';
import LineTagStyle from './LineTag.style.scss';

const LineTag = ({ title, width, height, bgClass, lineClass, className, ...rest }) => (
    <div className={classNames([
        LineTagStyle.lineTag,
        className
    ])}
        {...rest}
    >

        {/* Line 1 */}
        <div
            style={{ width, height }}
            className={classNames([lineClass, LineTagStyle.line])}
        />

        {/* Content */}
        <div
            className={classNames([bgClass, LineTagStyle.content])} >
            {title}
        </div>

        {/* Line 2 */}
        <div
            style={{ width, height }}
            // className={lineClass}
            className={classNames([lineClass, LineTagStyle.line])}
        />

    </div>
);

LineTag.propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    bgClass: PropTypes.string,
    lineClass: PropTypes.string
};

LineTag.defaultProps = {
    width: '25%',
    height: '1px',
    bgClass: 'bg-secondary',
    lineClass: 'bg-secondary'
};

export default LineTag;