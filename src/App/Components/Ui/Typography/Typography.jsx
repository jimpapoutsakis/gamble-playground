// Dependencies...
import { createElement } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import TypographyStyle from './Typography.style.scss';
import {
    GetBorderColor,
    GetBorderPosition,
    HasBorder
} from './Helpers/Helpers';

// Main Component...
const Typography = ({ tag, children, border, className, ...rest }) => {

    return createElement(
            tag,
            {
                className: classNames([
                    HasBorder(border),
                    className
                ]),
                ...rest
            },
            children
        );

};

// Default props....
Typography.defaultProps = {
    tag: "h1"
};

// Prop types...
Typography.propTypes = {
    tag: PropTypes.string,
    border: PropTypes.object
};

// Export...
export default Typography;