import PropTypes from 'prop-types';

const GetWordMargin = margin => ({
    marginTop: margin.top,
    marginRight: margin.right,
    marginBottom: margin.bottom,
    marginLeft: margin.left
});

const FirstLetter = ({ words, wordMargin, wordColor }) => (
    words.map((word, i) => (
        <span style={{ ...GetWordMargin(wordMargin) }} key={`textFx-word-${i}`} >
            <span className={`text-${wordColor}`} >
                {word[0]}
            </span>
            {word.substring(1)}
        </span>
    ))
);

const TextFx = ({ text, wordColor, wordMargin, target }) => {

    const words = text.split(' ');

    switch (target) {
        case 'first-letter':
            return (
                <FirstLetter
                    words={words}
                    wordMargin={wordMargin}
                    wordColor={wordColor}
                />
            );
            break;
       /*  case 'last-letter':
            break; */
        default:
            console.error(`TextFx: Unrecognized target ${target}`);
            break;
    }

};

TextFx.defaultProps = {
    wordColor: 'secondary',
    wordMargin: {
        top: '0px',
        right: '4px',
        bottom: '0px',
        left: '4px'
    },
    target: 'first-letter'
};

TextFx.propTypes = {
    wordColor: PropTypes.string,
    wordMargin: PropTypes.object,
    target: PropTypes.string
};

export default TextFx;