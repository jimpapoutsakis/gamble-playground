import TypographyStyle from '../Typography.style.scss';

const GetBorderColor = color => {
    switch (color) {
        case 'primary':
            return TypographyStyle.borderPrimaryColor;
            break;
        case 'secondary':
            return TypographyStyle.borderSecondaryColor;
            break;
        case 'success':
            return TypographyStyle.borderSuccessColor;
            break;
        case 'warning':
            return TypographyStyle.borderWarningColor;
            break;
        case 'info':
            return TypographyStyle.borderInfoColor;
            break;
        case 'danger':
            return TypographyStyle.borderDangerColor;
            break;
    }
};

const GetBorderPosition = position => {
    switch (position) {
        case 'top':
            return TypographyStyle.borderTopPosition;
            break;
        case 'right':
            return TypographyStyle.borderRightPosition;
            break;
        case 'bottom':
            return TypographyStyle.borderBottomPosition;
            break;
        case 'left':
            return TypographyStyle.borderLeftPosition;
            break;
    }
};

const HasBorder = border => {
    if ( border ) {
        return [
            TypographyStyle.hasBorder,
            GetBorderColor(border.color),
            GetBorderPosition(border.position),
        ];
    }
};

export {  
    GetBorderColor,
    GetBorderPosition,
    HasBorder
};