import PropTypes from 'prop-types';
import classNames from 'classnames';

const Hr = ({ color, width, className, ...rest }) => (
    <div className="row justify-content-center">
        <hr
            style={{ width }}
            className={classNames([
                className,
                `border border-${color} border-bottom-0`
            ])
            }
            {...rest}
        />
    </div>
);

Hr.defaultProps = {
    color: 'secondary',
    width: '50px'
};

Hr.propTypes = {
    color: PropTypes.string,
    width: PropTypes.string
};

export default Hr;