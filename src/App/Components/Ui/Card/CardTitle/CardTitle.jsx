import classNames from 'classnames';
import CardContext from '../CardContext';

const CardTitle = ({ children, className, ...rest }) => (
    <CardContext.Consumer>
        {({ style }) => (
            <div
                className={
                    classNames([
                        style.cardTitle,
                        className
                    ])
                }
                {...rest}
            >
                {children}
            </div>
        )}
    </CardContext.Consumer>
);

export default CardTitle;