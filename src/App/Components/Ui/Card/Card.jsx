import classNames from 'classnames';
import CardContext from './CardContext';
import CardStyle from './Card.style.scss';

const Card = ({ children, className, shadow, hover, ...rest }) => (
    <CardContext.Provider value={{ style: CardStyle }} >
        <div className={
            classNames([
                CardStyle.card,
                shadow == false && CardStyle.noShadow,
                hover == false && CardStyle.noHover,
                className
            ])
        }
            {...rest}
        >
            {children}
        </div>
    </CardContext.Provider>
);

export default Card;