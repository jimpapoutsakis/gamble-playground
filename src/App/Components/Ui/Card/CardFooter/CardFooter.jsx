import classNames from 'classnames/bind';
import CardContext from '../CardContext';

const CardFooter = ({ children, className, flow, ...rest }) => (
    <CardContext.Consumer>
        {({ style }) => (
            <div
                className={
                    classNames([
                        style.cardFooter,
                        className,
                        flow == 'right' && style.flowRight
                    ])
                }
                {...rest}
            >
                {children}
            </div>
        )}
    </CardContext.Consumer>
);

export default CardFooter;