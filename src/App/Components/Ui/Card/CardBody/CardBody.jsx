import classNames from 'classnames/bind';
import CardContext from '../CardContext';

const CardBody = ({ children, className, ...rest }) => (
    <CardContext.Consumer>
        {({ style }) => (
            <div
                className={
                    classNames([
                        style.cardBody,
                        className
                    ])
                }
                {...rest}
            >
                {children}
            </div>
        )}
    </CardContext.Consumer>
);

export default CardBody;