import PropTypes from 'prop-types';
import classNames from 'classnames';
import { GetOpacityClass } from './Helpers/Helpers';

const Overlay = ({ opacity, className, ...rest }) => (
    <div {...rest} className={
        classNames([
            'overlay',
            GetOpacityClass(opacity),
            className
        ])
    } />
);

Overlay.propTypes = {
    opacity: PropTypes.number
};

Overlay.defaultProps = {
    opacity: 0.5
};

export default Overlay;