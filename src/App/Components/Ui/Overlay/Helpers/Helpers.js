export const GetOpacityClass = opacity => {
    const opacityClass = opacity.toString().replace('.', '');
    return `opacity${opacityClass}`;
};