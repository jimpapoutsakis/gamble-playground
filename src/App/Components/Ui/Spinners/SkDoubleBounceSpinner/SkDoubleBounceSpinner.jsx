import classNames from 'classnames/bind';
import SkDoubleBounceSpinnerStyle from './SkDoubleBounceSpinner.style.scss';

const SkDoubleBounceSpinner = () => (
    <div className={classNames([SkDoubleBounceSpinnerStyle.skDoubleBounce])} >
        <div className={classNames([SkDoubleBounceSpinnerStyle.skCircle1, SkDoubleBounceSpinnerStyle.doubleBounce1])} />
        <div className={classNames([SkDoubleBounceSpinnerStyle.skCircle2, SkDoubleBounceSpinnerStyle.doubleBounce2])} />
    </div>
);

export default SkDoubleBounceSpinner;