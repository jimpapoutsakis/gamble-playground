
import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({ auth, ...rest }) => {
    console.log(auth, '??');
    return (
        auth.isAuthenticated === true
            ? <Redirect to={{
                pathname: '/dashboard',
                state: { from: rest.location }
            }} />
            : <Route {...rest} />
    );
};

export default PublicRoute;