import AppLayoutStyle from './AppLayout.style.scss';
import classNames from 'classnames';

const Main = ({ children, className, ...rest }) => (
    <main {...rest} className={classNames([AppLayoutStyle.main, className])}>
        { children }
    </main>
);

export { Main };