import * as GamblersTypes from "../Actions/Gamblers/GamblersTypes";

const initialState = {
    data: [],
    isSendingMessage: false
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case GamblersTypes.GET_GAMBLERS:
            return {
                ...state,
                isFetching: payload.isFetching
            };
        case GamblersTypes.GET_GAMBLERS_SUCCEED:
            return {
                ...state,
                isFetching: payload.isFetching,
                data: payload.gamblers
            };
        case GamblersTypes.GET_GAMBLERS_FAILED:
            return {
                ...state,
                isFetching: payload.isFetching,
            };
        case GamblersTypes.SEND_MESSAGE:
            return {
                ...state,
                isSendingMessage: payload.isSendingMessage,
            };
        case GamblersTypes.SEND_MESSAGE_SUCCEED:
            return {
                ...state,
                isSendingMessage: payload.isSendingMessage,
            };
    /*     case GamblersTypes.SEND_MESSAGE_FAILED:
            return {
                ...state,
                isSendingMessage: payload.isSendingMessage,
            }; */
        default:
            return state;
    }
};