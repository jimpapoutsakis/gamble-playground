import * as AuthTypes from "../Actions/Auth/AuthTypes";

const initialState = {
    user: {},
    isAuthenticated: false,
    isAuthenticating: false,
    isRetrievingUser: true
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case AuthTypes.AUTH_USER:
            return {
                ...state,
                isAuthenticating: payload.isAuthenticating
            };
        case AuthTypes.AUTH_USER_SUCCEED:
            return {
                ...state,
                isAuthenticating: payload.isAuthenticating,
                isAuthenticated: payload.isAuthenticated,
                user: payload.user
            };
        case AuthTypes.AUTH_USER_FAILED:
            return {
                ...state,
                isAuthenticated: payload.isAuthenticated,
                isAuthenticating: payload.isAuthenticating
            };
        case AuthTypes.AUTH_RETRIEVE_USER:
            return {
                ...state,
                isRetrievingUser: payload.isRetrievingUser
            };
        case AuthTypes.AUTH_RETRIEVE_USER_SUCCEED:
            return {
                ...state,
                isAuthenticating: payload.isAuthenticating,
                isAuthenticated: payload.isAuthenticated,
                user: payload.user,
                isRetrievingUser: payload.isRetrievingUser
            };
        case AuthTypes.AUTH_RETRIEVE_USER_FAILED:
            return {
                ...state,
                isRetrievingUser: payload.isRetrievingUser
            };
        default:
            return state;
    }
};