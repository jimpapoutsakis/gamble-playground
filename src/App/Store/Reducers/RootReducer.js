import { combineReducers } from "redux";
import AuthReducer from "../Reducers/AuthReducer";
import gamblersReducer from '../Reducers/GamblersReducer';

export default combineReducers({
    auth: AuthReducer,
    gamblers: gamblersReducer
});