import { createStore, applyMiddleware, compose } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from "./Reducers/RootReducer";

const initialState = {};

class Store {
    constructor() {
        this.store = {};
    }

    Create(apolloClient) {
        const middleware = [thunkMiddleware];
       
        this.store = createStore(
            rootReducer,
            initialState,
            compose(
                applyMiddleware(...middleware),
                window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
            )
        );
    }

    Configure(nextRootReducer) {
        this.store.replaceReducer(nextRootReducer);
    }

    Get() {
        return this.store;
    }
}

export default Store;