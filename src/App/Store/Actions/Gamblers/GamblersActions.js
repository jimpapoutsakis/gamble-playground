import * as GamblersTypes from "./GamblersTypes";
import gql from "graphql-tag";
import apolloClient from '$ApolloClient/ApolloClient';
import { ExtractGraphQLErrors } from "$Helpers";

// Get Gamblers...
const GetGamblers = (username, password) => async dispatch => {

    dispatch({
        type: GamblersTypes.GET_GAMBLERS,
        payload: {
            isFetching: true
        }
    });

    let payload = null;

    // GraphQL Login Mutation...
    const gamblersQuery = gql`
    query {
         users(type: "Gambler") {
            firstName,
            lastName,
            avatar,
            level
         }
    }`;

    try {

        const gamblersResponse = await apolloClient.query({ query: gamblersQuery });
        const { users: gamblers } = gamblersResponse.data;

        dispatch({
            type: GamblersTypes.GET_GAMBLERS_SUCCEED,
            payload: {
                isFetching: false,
                gamblers 
            }
        });

        return 'succeed';

    } catch (err) {
        console.log(err);

        const graphQLErrors = ExtractGraphQLErrors(err.graphQLErrors);

        dispatch({
            type: GamblersTypes.GET_GAMBLERS_FAILED,
            payload: {
                isFetching: false
            }
        });

        return graphQLErrors;

    }

};

// Send Message...
const SendMessage = msg => async dispatch => {

    dispatch({
        type: GamblersTypes.SEND_MESSAGE,
        payload: {
            isSendingMessage: true
        }
    });

    let payload = null;

    // GraphQL Login Mutation...
    const gamblersQuery = gql`
    query {
         users(type: "Gambler") {
            firstName,
            lastName,
            avatar,
            level
         }
    }`;

    try {

        const gamblersResponse = await apolloClient.query({ query: gamblersQuery });
        const { users: gamblers } = gamblersResponse.data;

        dispatch({
            type: GamblersTypes.GET_GAMBLERS_SUCCEED,
            payload: {
                isFetching: false,
                gamblers 
            }
        });

        return 'succeed';

    } catch (err) {
        console.log(err);

        const graphQLErrors = ExtractGraphQLErrors(err.graphQLErrors);

        dispatch({
            type: GamblersTypes.GET_GAMBLERS_FAILED,
            payload: {
                isFetching: false
            }
        });

        return graphQLErrors;

    }

};

// Exports...
export { GetGamblers, SendMessage };
