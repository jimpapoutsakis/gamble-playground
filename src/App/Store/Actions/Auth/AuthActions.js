import * as AuthTypes from "./AuthTypes";
import gql from "graphql-tag";
import apolloClient from '$ApolloClient/ApolloClient';
import { ExtractGraphQLErrors } from "$Helpers";

const AuthUser = (username, password) => async dispatch => {

    dispatch({
        type: AuthTypes.AUTH_USER,
        payload: {
            isAuthenticating: true
        }
    });

    let payload = null;

    // GraphQL Login Mutation...
    const loginMutation = gql`
    mutation {
         login(username: "${username}", password: "${password}") {
             user {
                firstName,
                lastName,
                avatar,
                level,
                type
             }
         }
    }`;

    let authResponse = null;

    try {

        authResponse = await apolloClient.mutate({ mutation: loginMutation });
        const { user } = authResponse.data.login;

        const payload = {
            isAuthenticating: false,
            isAuthenticated: true,
            user
        };

        dispatch({
            type: AuthTypes.AUTH_USER_SUCCEED,
            payload
        });

        sessionStorage.setItem('auth', JSON.stringify(payload));

        return 'succeed';

    } catch (err) {

        const graphQLErrors = ExtractGraphQLErrors(err.graphQLErrors);

        dispatch({
            type: AuthTypes.AUTH_USER_FAILED,
            payload: {
                isAuthenticating: false,
                isAuthenticated: false,
            }
        });

        return graphQLErrors;

    }

};

const GetAuth = () => (dispatch, getState) => {

    dispatch({
        type: AuthTypes.AUTH_RETRIEVE_USER,
        payload: {
            isRetrievingUser: true
        }
    });

    const cachedAuth = sessionStorage.getItem("auth");

    if (cachedAuth !== null) {

        let parsedAuth = JSON.parse(cachedAuth);

        // Retrieve succeed...
        dispatch({
            type: AuthTypes.AUTH_RETRIEVE_USER_SUCCEED,
            payload: {
                isAuthenticated: parsedAuth.isAuthenticated,
                isAuthenticating: false,
                user: parsedAuth.user,
                isRetrievingUser: false
            }
        });

    } else {

        // Retrieve failed...
        dispatch({
            type: AuthTypes.AUTH_RETRIEVE_USER_FAILED,
            payload: {
                isRetrievingUser: false
            }
        });

    }

};

export { AuthUser, GetAuth };
