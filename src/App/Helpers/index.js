const ExtractGraphQLErrors = errors =>
    errors.map(({ message }) => `[ ! ] ${message} \n`).toString();

// Exports...
export {
    ExtractGraphQLErrors
};