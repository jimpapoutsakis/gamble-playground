// Deps...
import { Component } from "react";
import classNames from "classnames";
import { BlockUi } from "$Components/Ui/Modified/BlockUi";
import { SkDoubleBounceSpinner } from "$Components/Ui/Spinners";
import { SweetAlert } from "$Components/Ui/Modified/SweetAlert";
import { connect } from 'react-redux';

// Child Components...
import Title from "./Title/Title";
import Form from "./Form/Form";

import { Query, graphql } from "react-apollo";
import { AuthUser } from '$Store/Actions/Auth/AuthActions';

// Main Component...
class LoginPage extends Component {
    state = {
        blocking: false,
        sweetAlert: {
            show: false,
            text: "",
        },
        form: {
            username: "",
            password: ""
        }
    };

    HandleInput = ({ target: { name, value } }) => {
        this.setState(state => ({
            form: {
                ...state.form,
                [name]: value
            }
        }));
    };

    HandleSubmit = () => {

        // Activate BlockUi...
        this.setState(state => ({
            blocking: true
        }));

        // Extract required data...
        const { username, password } = this.state.form;

        // User authentication callback...
        const callback = this.props.AuthUser(username, password).then(callback => {

            if (callback == 'succeed') {

                this.setState({
                    blocking: false
                });

                this.props.history.push('/dashboard');

            } else {

                this.setState(state => ({
                    blocking: false,
                    sweetAlert: {
                        ...state.sweetAlert,
                        show: true,
                        text: callback
                    }
                }));

            }

        });

    };

    GetBlockUiProps = () => ({
        blocking: this.state.blocking,
        loader: <SkDoubleBounceSpinner />
    });

    GetSweetAlertProps = () => ({
        show: this.state.sweetAlert.show,
        title: "Login Was Failed",
        type: 'error',
        text: this.state.sweetAlert.text,
        onConfirm: this.CloseSweetAlert
    });

    CloseSweetAlert = () => {
        this.setState(state => ({
            sweetAlert: {
                ...state.sweetAlert,
                show: false
            }
        }));
    };

    GenerateFormProps = () => this.state.form;

    render() {
        return (
            <BlockUi tag="div" {...this.GetBlockUiProps()}>
                <div className="container">
                    <SweetAlert {...this.GetSweetAlertProps()} />

                    {/* Title Row... */}
                    <div className="row">
                        <div className="col">
                            <div className="d-flex flex-column justify-content-center align-items-center p-5">
                                <Title />
                            </div>
                        </div>
                    </div>

                    {/* Form Row... */}
                    <div className="row justify-content-center">
                        <div className="col-md-6 col-sm-12 col-xs-12">
                            <Form
                                fields={this.GenerateFormProps()}
                                HandleInput={this.HandleInput}
                                HandleSubmit={this.HandleSubmit}
                            />
                        </div>
                    </div>
                </div>
            </BlockUi>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    AuthUser: (username, password) => dispatch(AuthUser(username, password))
});

export default connect(null, mapDispatchToProps)(LoginPage);