// Deps...
import { CircleTag } from '$Components/Ui/Tags';
import { Typography, TextFx } from '$Components/Ui/Typography';

const Title = () => (
    <>
        <CircleTag
            bgColorClass=""
            borderStyle="dashed"
            padding="70px"
        >
            <i className="fa fa-sign-in text-secondary fa-6x" />
        </CircleTag>

        <Typography
            tag="div"
            className='display-3 pt-2 pb-2 prevent-select'
        >

            <TextFx
                text="Gamble Playground"
                wordColor="secondary"
            />

        </Typography>

        <Typography
            tag="div"
            className='display-4 prevent-select'
        >

            <TextFx
                text="Login To Our Platform"
                wordColor="secondary"
            />

        </Typography>
    </>
);

export default Title;