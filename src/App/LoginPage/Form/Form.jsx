// Deps...
import Validate from "react-validate-form";
import { TextInput } from "react-materialize";
import { Button } from "$Components/Ui/Button";

// Main Component...
const Form = ({ HandleInput, HandleSubmit, fields, ...rest }) => {
    const GetValidations = () => ({
        username: ["required"],
        password: ["required"]
    });

    const GetRules = () => ({
        required: {
            message: field => `This field is required.`
        }
    });

    return (
        <Validate rules={GetRules()} validations={GetValidations()}>
            {({ validate, errorMessages }) => (
                <form
                    onSubmit={e => {
                        e.preventDefault();
                        HandleSubmit();
                    }}
                >
                    {/* First row... */}
                    <div className="form-group">
                        <div className="row">
                            <div className="col">
                                <TextInput
                                    value={fields.username}
                                    name="username"
                                    label="Username"
                                    onChange={e => {
                                        validate(e);
                                        HandleInput(e);
                                    }}
                                    required
                                />
                                <p className="text-danger">
                                    {errorMessages.username && errorMessages.username[0]}
                                </p>
                            </div>
                        </div>
                    </div>

                    <div className="form-group">
                        <div className="row">
                            <div className="col">
                                <TextInput
                                    value={fields.password}
                                    name="password"
                                    label="Password"
                                    type="password"
                                    onChange={e => {
                                        validate(e);
                                        HandleInput(e);
                                    }}
                                    required
                                />
                                <p className="text-danger">
                                    {errorMessages.password && errorMessages.password[0]}
                                </p>
                            </div>
                        </div>
                    </div>


                    {/* Submit Button... */}
                    <div className="text-center">
                        <Button size="lg" variant="secondary">
                            <i className="fa fa-sign-in" /> Submit
                        </Button>
                    </div>
                </form>
            )}
        </Validate>
    );
};

export default Form;