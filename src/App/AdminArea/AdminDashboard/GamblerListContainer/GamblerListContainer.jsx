// Deps...
import { Component } from 'react';
import { connect } from 'react-redux';
import { GetGamblers, SendMessage } from '$Store/Actions/Gamblers/GamblersActions';

// Child Components...
import GamblerList from './GamblerList/GamblerList';

// Main Component...
class GamblerListContainer extends Component {

    componentDidMount() {
        this.props.GetGamblers();
    };

    render() {
        const { isFetching, gamblers } = this.props;

        return (
            <div className="p-3">
                <div className="container-fluid">
                    <div className="row">
                        {!isFetching && <GamblerList gamblers={gamblers} />}
                    </div>
                </div>
            </div>
        );
    }
};

// Redux...
const mapStateToProps = state => ({
    isFetching: state.gamblers.isFetching,
    gamblers: state.gamblers.data
});

const mapDispatchToProps = dispatch => ({
    GetGamblers: () => dispatch(GetGamblers()),
    SendMessage: () => dispatch(SendMessage())
});

// Exports...
export default connect(mapStateToProps, mapDispatchToProps)(GamblerListContainer);