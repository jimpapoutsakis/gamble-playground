// Deps...
import { CircleTag } from '$Components/Ui/Tags';
import { Card, CardTitle, CardBody, CardFooter } from '$Components/Ui/Card';
import { Button } from "$Components/Ui/Button";
import { Textarea } from 'react-materialize';

// Main Component...
const GamblerList = ({ gamblers }) => {

    return (
        gamblers.map((gambler, i) => (
            <div className="col-md-4 col-xs-12 col-sm-12 p-3" key={`gambler-${i}`}>
                <Card className="bg-primary-light p-2 text-center">
                    <CardTitle className="d-flex justify-content-center p-2">
                        <CircleTag
                            bgColorClass=""
                            borderStyle="dashed"
                            width="70px"
                            height="70px"
                        >
                            <img width="64" height="64" className="circle" src={gambler.avatar} />
                        </CircleTag>
                    </CardTitle>
                    <CardBody className="p-2">
                        {`${gambler.firstName} - ${gambler.lastName}`}
                        <Textarea
                            name="message"
                            label="Message"
                            onChange={e => { validate(e); HandleInput(e); }}
                        />
                    </CardBody>
                    <CardFooter className="p-2" flow="right">
                        <Button variant="secondary">
                            <i className="fa fa-send-o" /> Send Message
                            </Button>
                    </CardFooter>
                </Card>
            </div>
        ))
    );
};

// Exports...
export default GamblerList;