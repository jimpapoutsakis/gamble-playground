// Deps...
import { SideNav, SideNavItem, Button } from "react-materialize";
import { connect } from "react-redux";
import Ink from "react-ink";
import { Switch, Route } from "react-router-dom";

// Style...
import AdminDashboardStyle from './AdminDashboard.style.scss';

// Child Components...
import GamblerListContainer from './GamblerListContainer/GamblerListContainer';

// Main Component...
const AdminDashboard = ({ user, match, history }) => {
    const Navigate = url => {
        history.push(`${match.url}/${url}`);
    };

    return (
        <div className={AdminDashboardStyle.adminDashboard}>

            <div className={AdminDashboardStyle.sideNavContainer} >
                <SideNav fixed options={{ closeOnClick: true }}>
                    <SideNavItem
                        userView
                        user={{
                            image: user.avatar,
                            name: `${user.firstName} ${user.lastName}`,
                            email: user.type
                        }}
                    />
                    <SideNavItem href="#" onClick={() => Navigate("gamblers")}>
                        <Ink />
                        <i className="fa fa-users fa-2x" /> Gamblers
            </SideNavItem>
                    <SideNavItem href="#">
                        <Ink />
                        <i className="fa fa-gamepad fa-2x" /> Games
            </SideNavItem>
                </SideNav>
            </div>

            <div className={AdminDashboardStyle.content} >
                <Switch>
                    <Route path="/" component={GamblerListContainer} />
                </Switch>
            </div>

        </div>
    );
};

// Redux...
const mapStateToProps = state => ({
    user: state.auth.user
});

// Exports...
export default connect(
    mapStateToProps,
    {}
)(AdminDashboard);
