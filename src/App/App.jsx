// Deps...
import { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { GetAuth } from '$Store/Actions/Auth/AuthActions';

// Layout...
import { Main } from './AppLayout/AppLayout';

// Style...
import AppStyle from './App.style.scss';

// Child Components...
import LoginPage from './LoginPage/LoginPage';
import UserDashboard from './UserArea/UserDashboard';
import AdminDashboard from './AdminArea/AdminDashboard/AdminDashboard';
import ProtectedRoute from '$Components/Security/ProtectedRoute';

// Main Component...
class App extends Component {

  componentDidMount() {
    this.props.GetAuth();
  }

  RenderDashboardByUser = () => {
    const { auth } = this.props;

     return (
      !auth.isRetrievingUser &&
        auth.user.type == 'gambler'
        ? <Route
          path='/dashboard'
          render={routeProps => <UserDashboard auth={auth} {...routeProps} /> }
        />
        : <Route
          path='/dashboard'
          render={routeProps => <AdminDashboard auth={auth} {...routeProps} /> }
        />
    );


    /* return (
      !auth.isRetrievingUser &&
        auth.user.type == 'gambler' ?
        <ProtectedRoute
          auth={auth}
          path='/dashboard'
          component={UserDashboard}
        />
        : <ProtectedRoute
          auth={auth}
          path='/dashboard'
          component={AdminDashboard}
        />
    ); */
  }

  render() {
    return (
      <div className={AppStyle.app}>
        <Main>
          <Switch>
            <Route exact path="/" component={LoginPage} />
            {/* <Route path="/dashboard" component={Dashboard} /> */}
            {this.RenderDashboardByUser()}
          </Switch>
        </Main>
      </div>
    );
  }

};

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  GetAuth: () => dispatch(GetAuth())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);