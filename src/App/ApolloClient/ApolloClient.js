import { ApolloClient, HttpLink, InMemoryCache } from "apollo-boost";

const httpLink = new HttpLink({ uri: "http://localhost:3000/graphql" });

const apolloClient = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache()
});

export default apolloClient;