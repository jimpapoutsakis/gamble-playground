/* Deps... */
import { render } from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";
import * as serviceWorker from './serviceWorker';
import { Provider } from "react-redux";

// Redux Store...
import Store from "./App/Store/Store";

// Apollo Client
import { ApolloProvider } from "react-apollo";
import apolloClient from './App/ApolloClient/ApolloClient';

// Materialize Js...
import '$assets/materialize/js/bin/materialize.min';

// Font awesome...
import '$fonts/fontawesome-free-5.7.2-web/css/v4-shims.css';
import '$fonts/fontawesome-free-5.7.2-web/css/all.css';

// Style...
import './index.scss';

// Redux Init...
const store = new Store();
store.Create();

// Main...
if (module.hot) {

    module.hot.accept();

    const NextApp = require("./App/App").default;

    render(
        <ApolloProvider client={apolloClient}>
            <Provider store={store.Get()} >
                <Router>
                    <NextApp />
                </Router>
            </Provider>
        </ApolloProvider>,
        document.getElementById('root')
    );

}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
