This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Resources
1. Bootstrap 4 (https://getbootstrap.com/)
2. Materialize.css (https://materializecss.com/)
3. ReactMaterialize (http://react-materialize.github.io/react-materialize/?path=/story/react-materialize--welcome)
4. Ghrelin Restaurant ( soon to release React web template )

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.