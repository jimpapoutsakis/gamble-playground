const merge = require("webpack-merge");
const { resolve } = require("path");

const commonConfig = require("./common");

const CUSTOM_PATHS = require("./customPaths");

const devConfig = merge(commonConfig, {
    entry: [
        "react-hot-loader", // activate HMR for React
        "webpack-dev-server/client?http://localhost:8080", // bundle the client for webpack-dev-server and connect to the provided endpoint
        "webpack/hot/only-dev-server", // bundle the client for hot reloading, only- means to only hot reload for successful updates
        resolve(__dirname, CUSTOM_PATHS.SRC + "/index")
    ],
    mode: "development",
    devtool: "source-map",
    output: {
        path: resolve(__dirname, CUSTOM_PATHS.DIST),
        filename: "[name].bundle.js",
        chunkFilename: '[name].chunk.js',
        publicPath: "/",
    },
    devServer: {
        hot: true,
        historyApiFallback: true
    }
});

module.exports = devConfig;
