const { join } = require('path');

const CSSLoader = {
    loader: "css-loader",
    options: {
        modules: false,
        sourceMap: true,
        minimize: true
    }
};

const CSSModuleLoader = {
    loader: "css-loader",
    options: {
        modules: true,
        sourceMap: true,
        localIdentName: "[local]__[hash:base64:5]",
        minimize: true
    }
};

const PostCSSLoader = {
    loader: 'postcss-loader',
    options: {
        config: {
            path: join(process.cwd(), 'configs', 'postcss.config')
        }
    }
};

module.exports = {
    CSSLoader,
    CSSModuleLoader,
    PostCSSLoader
};