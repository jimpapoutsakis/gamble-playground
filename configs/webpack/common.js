const webpack = require("webpack");

/* [-]Plugin imports begin */
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { resolve, join } = require("path");
/* Plugin imports end */

/* [-]Custom imports begin */
const CUSTOM_PATHS = require("./customPaths");
const { CSSLoader, CSSModuleLoader, PostCSSLoader } = require("./loaders");
/* Custom imports end */

const commonConfig = {
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: ["babel-loader", "react-hot-loader/webpack"],
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ["style-loader", CSSLoader]
            },
            {
                test: /\.(scss$)/,
                exclude: [/\.style\.scss$/],
                // exclude: [/node_modules/, /\.style\.scss$/],
                use: [
                    "style-loader", 
                    CSSLoader, 
                    "sass-loader",
                    {
                        loader: "sass-resources-loader",
                        options: {
                            resources: [...require(join(process.cwd(), "src", "sassResources"))]
                        }
                    }
                ]
            },
            {
                test: /\.style\.scss$/,
                exclude: /node_modules/,
                use: [
                    "style-loader",
                    CSSModuleLoader,
                    PostCSSLoader,
                    "sass-loader",
                    {
                        loader: "sass-resources-loader",
                        options: {
                            resources: [...require(join(process.cwd(), "src", "sassResources"))]
                        }
                    }
                ]
            },
            {
                test: /\.(scss$)|(style.scss$)/,
                loader: "import-glob-loader"
            },
            {
                test: /\.svg$/,
                use: ["svg-loader"]
            },
            {
                test: /\.(jpe?g|png|gif|eot|svg)$/i,
                loaders: [
                    "file-loader?hash=sha512&digest=hex&name=img/[hash].[ext]",
                    "image-webpack-loader?bypassOnDebug&optipng.optimizationLevel=7&gifsicle.interlaced=false"
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file-loader?hash=sha512&digest=hex&name=font/[hash].[ext]"
            }
        ]
    },
    resolve: {
        extensions: ["*", ".json", ".js", ".jsx", ".css"],
        alias: {
            "react-dom": "@hot-loader/react-dom",
            $src: resolve(__dirname, CUSTOM_PATHS.SRC),
            "~src": resolve(__dirname, CUSTOM_PATHS.SRC),
            $Components: resolve(__dirname, CUSTOM_PATHS.COMPONENTS),
            $assets: resolve(__dirname, CUSTOM_PATHS.ASSETS),
            $fonts: resolve(__dirname, CUSTOM_PATHS.FONTS),
            $images: resolve(__dirname, CUSTOM_PATHS.IMAGES),
            $vendors: resolve(__dirname, CUSTOM_PATHS.VENDORS),
            $Store: resolve(__dirname, CUSTOM_PATHS.STORE),
            $ApolloClient: resolve(__dirname, CUSTOM_PATHS.APOLLO_CLIENT),
            $Helpers: resolve(__dirname, CUSTOM_PATHS.HELPERS)
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            React: "react",
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new HtmlWebpackPlugin({
            template: join(CUSTOM_PATHS.PUBLIC, "index.html")
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin() // prints more readable module names in the browser console on HMR updates
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                            commons: {
                                    // this takes care of all the vendors in your files
                                    // no need to add as an entrypoint.
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    }
};

module.exports = commonConfig;
