const merge = require("webpack-merge");
const { resolve } = require("path");

const commonConfig = require("./common");

const prodConfig = merge(commonConfig, {
    entry: resolve(__dirname, "../../src/index"),
    mode: "production",
    output: {
        crossOriginLoading: 'anonymous',
        filename: "js/bundle.[hash].min.js",
        path: resolve(__dirname, "../../dist"),
        publicPath: "/"
    },
    plugins: []
});

module.exports = prodConfig;