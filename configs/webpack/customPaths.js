const CUSTOM_PATHS = {
    SRC: "../../src",
    COMPONENTS: "../../src/App/Components",
    ASSETS: "../../src/assets",
    FONTS: "../../src/assets/fonts",
    IMAGES: "../../src/assets/images",
    DIST: "dist/",
    PUBLIC: "public",
    VIRTUAL: "virtual",
    VENDORS: '../../src/assets/vendors',
    STORE: "../../src/App/Store",
    APOLLO_CLIENT: "../../src/App/ApolloClient",
    HELPERS: "../../src/App/Helpers"
};

module.exports = CUSTOM_PATHS;